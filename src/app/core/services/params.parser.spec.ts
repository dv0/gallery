import { HttpParams } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { ParamsParser } from './params.parser';

import { environment } from 'src/environments/environment';

describe('ParamsParser', () => {
  let service: ParamsParser;

  beforeEach(async () => {
    service = TestBed.inject(ParamsParser);
  });

  it('should parse params with offset 0', () => {
    const params = new HttpParams()
      .append('offset', 0)
      .append('api_key', environment.apiKey)
      .append('limit', 9)
      .append('rating', 'g');

    const parsedParams = service.paramsParser(1);

    expect(parsedParams).toEqual(params);
  });

  it('should parse params with offset NOT equal to 0', () => {
    const params = new HttpParams()
      .append('offset', 18)
      .append('api_key', environment.apiKey)
      .append('limit', 9)
      .append('rating', 'g');

    const parsedParams = service.paramsParser(3);

    expect(parsedParams).toEqual(params);
  });
});
