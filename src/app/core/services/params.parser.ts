import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ParamsParser {
  public paramsParser(page: number): HttpParams {
    const offset = this.getOffset(page);

    return new HttpParams()
      .append('offset', offset)
      .append('api_key', environment.apiKey)
      .append('limit', 9)
      .append('rating', 'g');
  }

  private getOffset(page: number): number {
    return page === 1 ? 0 : 9 * (page - 1);
  }
}
