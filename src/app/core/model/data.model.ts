// More info at: https://developers.giphy.com/docs/api/schema/
export interface IData {
  data: IGif[];
  pagination: IPagination;
  meta: IMeta;
}

export interface IPagination {
  offset: number;
  total_count: number;
  count: number;
}

export interface IGif {
  type: string;
  id: string;
  slug: string;
  url: string;
  bitly_url: string;
  embed_url: string;
  username: string;
  source: string;
  rating: string;
  content_url: string;
  source_tld: string;
  source_post_url: string;
  update_datetime: string;
  create_datetime: string;
  import_datetime: string;
  trending_datetime: string;
  images: IImages;
  title: string;
}

export interface IMeta {
  msg: string;
  status: number;
  response_id: string;
}

export interface IImages {
  fixed_height: IRendition;
}

export interface IRendition {
  frames: string;
  hash: string;
  height: string;
  mp4: string;
  mp4_size: string;
  size: string;
  url: string;
  webp: string;
  webp_size: string;
  width: string;
}
