import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';

import { IData } from '../model/data.model';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DataGateway {
  constructor(private http: HttpClient) {}

  public getTrendingListData$(params: HttpParams): Observable<IData> {
    return this.http.get<IData>(environment.apiUrl, { params });
  }

  public getSearchListData$(params: HttpParams): Observable<IData> {
    return this.http.get<IData>(environment.apiSearchUrl, { params });
  }
}
