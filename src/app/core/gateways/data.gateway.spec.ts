import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpParams, HttpRequest } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { DataGateway } from './data.gateway';

import { IData } from '../model/data.model';

import { environment } from 'src/environments/environment';

const apiKey: string = 'testing_api_key';

const dataResponse: IData = {
  data: [],
  pagination: {
    offset: 0,
    total_count: 2500,
    count: 9,
  },
  meta: {
    msg: 'OK',
    status: 200,
    response_id: 'response',
  },
};

describe('DataGateway', () => {
  let service: DataGateway;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
    });
    service = TestBed.inject(DataGateway);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should make get trending list data call', () => {
    const params = new HttpParams()
      .append('offset', 0)
      .append('api_key', apiKey)
      .append('limit', 9)
      .append('rating', 'g');

    service
      .getTrendingListData$(params)
      .subscribe((data: IData) => expect(data).toBe(dataResponse));

    const request = httpTestingController.expectOne(
      (req: HttpRequest<IData>) => {
        expect(req.url).toBe(`${environment.apiUrl}`);
        expect(req.method).toEqual('GET');
        expect(req.params.get('offset')).toEqual('0');
        expect(req.params.get('api_key')).toBe(apiKey);
        expect(req.params.get('rating')).toEqual('g');
        expect(req.params.get('limit')).toEqual('9');

        return true;
      }
    );
    expect(request.request.method).toEqual('GET');

    request.flush(dataResponse);
  });

  it('should make get search list data call', () => {
    const params = new HttpParams()
      .append('offset', 0)
      .append('api_key', apiKey)
      .append('limit', 9)
      .append('rating', 'g')
      .append('q', 'cheese');

    service
      .getSearchListData$(params)
      .subscribe((data: IData) => expect(data).toBe(dataResponse));

    const request = httpTestingController.expectOne(
      (req: HttpRequest<IData>) => {
        expect(req.url).toBe(`${environment.apiSearchUrl}`);
        expect(req.method).toEqual('GET');
        expect(req.params.get('offset')).toEqual('0');
        expect(req.params.get('api_key')).toBe(apiKey);
        expect(req.params.get('rating')).toEqual('g');
        expect(req.params.get('limit')).toEqual('9');
        expect(req.params.get('q')).toEqual('cheese');

        return true;
      }
    );
    expect(request.request.method).toEqual('GET');

    request.flush(dataResponse);
  });
});
