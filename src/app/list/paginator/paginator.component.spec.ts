import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginatorComponent } from './paginator.component';

import { ListService } from '../service/list.service';

import { IPagination } from 'src/app/core/model/data.model';

const paginationData: IPagination = {
  offset: 1,
  total_count: 200,
  count: 9,
};

describe('PaginatorComponent', () => {
  let component: PaginatorComponent;
  let fixture: ComponentFixture<PaginatorComponent>;
  let listServiceSpy: jasmine.SpyObj<ListService>;

  beforeEach(async () => {
    listServiceSpy = jasmine.createSpyObj('ListService', [
      'getRequestSearchTerm',
      'setListListParams',
    ]);

    await TestBed.configureTestingModule({
      declarations: [PaginatorComponent],
      providers: [
        {
          provide: ListService,
          useValue: listServiceSpy,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    listServiceSpy = TestBed.inject(ListService) as jasmine.SpyObj<ListService>;
    fixture = TestBed.createComponent(PaginatorComponent);
    component = fixture.componentInstance;
  });

  it('should set list params on page change', () => {
    component.paginationData = paginationData;
    fixture.detectChanges();

    listServiceSpy.getRequestSearchTerm.and.returnValue('testing value');
    component.onPageChange(10);

    expect(listServiceSpy.getRequestSearchTerm).toHaveBeenCalledTimes(1);
    expect(listServiceSpy.setListListParams).toHaveBeenCalledOnceWith({
      page: 10,
      searchTerm: 'testing value',
    });
  });
});
