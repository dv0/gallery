import { Component, Input } from '@angular/core';

import { ListService } from '../service/list.service';

import { IPagination } from 'src/app/core/model/data.model';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent {
  @Input() paginationData!: IPagination;

  constructor(private listService: ListService) {}

  public onPageChange(page: number): void {
    const searchTerm = this.listService.getRequestSearchTerm();

    this.listService.setListListParams({
      page: page,
      searchTerm: searchTerm,
    });
  }
}
