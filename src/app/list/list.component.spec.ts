import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComponent } from './list.component';
import { ListService } from './service/list.service';

import { IGif } from '../core/model/data.model';

const testingGif: IGif = {
  type: 'testing-image',
  id: 'testing-image',
  slug: 'testing-image',
  url: 'testing-image',
  bitly_url: 'testing-image',
  embed_url: 'testing-image',
  username: 'testing-image',
  source: 'testing-image',
  rating: 'testing-image',
  content_url: 'testing-image',
  source_tld: 'testing-image',
  source_post_url: 'testing-image',
  update_datetime: 'testing-image',
  create_datetime: 'testing-image',
  import_datetime: 'testing-image',
  trending_datetime: 'testing-image',
  images: {
    fixed_height: {
      frames: 'testing-image',
      hash: 'testing-image',
      height: 'testing-image',
      mp4: 'testing-image',
      mp4_size: 'testing-image',
      size: 'testing-image',
      url: 'testing-image',
      webp: 'testing-image',
      webp_size: 'testing-image',
      width: 'testing-image',
    },
  },
  title: 'testing-image',
};

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let listServiceSpy: jasmine.SpyObj<ListService>;

  beforeEach(async () => {
    listServiceSpy = jasmine.createSpyObj('ListService', ['getListData$']);

    await TestBed.configureTestingModule({
      declarations: [ListComponent],
      providers: [
        {
          provide: ListService,
          useValue: listServiceSpy,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    listServiceSpy = TestBed.inject(ListService) as jasmine.SpyObj<ListService>;
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should get gif image url', () => {
    const gifUrl = component.getGifUrl(testingGif);

    expect(gifUrl).toBe(testingGif.url);
  });
});
