import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, switchMap } from 'rxjs';

import { ListParams } from '../model/list-params.model';

import { DataGateway } from 'src/app/core/gateways/data.gateway';
import { ParamsParser } from 'src/app/core/services/params.parser';

import { IData } from 'src/app/core/model/data.model';

@Injectable({
  providedIn: 'root',
})
export class ListService {
  private listParams$ = new BehaviorSubject<ListParams>({
    page: 1,
    searchTerm: '',
  });

  public listData$: Observable<IData> = this.listParams$.pipe(
    switchMap((params: ListParams) => {
      if (params.searchTerm) {
        const parsedParams = this.paramsParser
          .paramsParser(params.page)
          .append('q', params.searchTerm);

        return this.dataGateway.getSearchListData$(parsedParams);
      } else {
        return this.dataGateway.getTrendingListData$(
          this.paramsParser.paramsParser(params.page)
        );
      }
    })
  );

  constructor(
    private dataGateway: DataGateway,
    private paramsParser: ParamsParser
  ) {}

  public setListListParams(params: ListParams): void {
    this.listParams$.next(params);
  }

  public getRequestSearchTerm(): string | null {
    return this.listParams$.value.searchTerm;
  }
}
