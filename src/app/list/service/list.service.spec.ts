import { HttpParams } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { ListService } from './list.service';

import { ListParams } from '../model/list-params.model';

import { DataGateway } from 'src/app/core/gateways/data.gateway';
import { ParamsParser } from 'src/app/core/services/params.parser';

import { IData } from 'src/app/core/model/data.model';

const apiKey: string = 'testing_api_key';

const params = new HttpParams()
  .append('offset', 4)
  .append('api_key', apiKey)
  .append('limit', 9)
  .append('rating', 'g');

const dataResponse: IData = {
  data: [],
  pagination: {
    offset: 0,
    total_count: 200,
    count: 1,
  },
  meta: {
    msg: 'OK',
    status: 200,
    response_id: 'response',
  },
};

describe('ListService', () => {
  let service: ListService;
  let paramsParserSpy: jasmine.SpyObj<ParamsParser>;
  let dataGatewaySpy: jasmine.SpyObj<DataGateway>;

  beforeEach(() => {
    paramsParserSpy = jasmine.createSpyObj('ParamsParser', ['paramsParser']);
    dataGatewaySpy = jasmine.createSpyObj('DataGateway', [
      'getTrendingListData$',
      'getSearchListData$',
    ]);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: ParamsParser,
          useValue: paramsParserSpy,
        },
        {
          provide: DataGateway,
          useValue: dataGatewaySpy,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    service = TestBed.inject(ListService);
    paramsParserSpy = TestBed.inject(
      ParamsParser
    ) as jasmine.SpyObj<ParamsParser>;
    dataGatewaySpy = TestBed.inject(DataGateway) as jasmine.SpyObj<DataGateway>;
  });

  it('should get list data', () => {
    dataGatewaySpy.getTrendingListData$.and.returnValue(of(dataResponse));
    paramsParserSpy.paramsParser.and.returnValue(params);

    service.listData$.subscribe((data: IData) =>
      expect(data).toBe(dataResponse)
    );
    expect(dataGatewaySpy.getTrendingListData$).toHaveBeenCalledTimes(1);
    expect(paramsParserSpy.paramsParser).toHaveBeenCalledTimes(1);
  });

  it('should get search list data', () => {
    const listParams: ListParams = { page: 1, searchTerm: 'cheese' };
    paramsParserSpy.paramsParser.and.returnValue(params);
    dataGatewaySpy.getSearchListData$.and.returnValue(of(dataResponse));

    service.setListListParams(listParams);
    service.listData$.subscribe((data: IData) =>
      expect(data).toBe(dataResponse)
    );
    expect(dataGatewaySpy.getSearchListData$).toHaveBeenCalledTimes(1);
    expect(paramsParserSpy.paramsParser).toHaveBeenCalledTimes(1);
  });

  it('should get request search term', () => {
    const listParams: ListParams = { page: 1, searchTerm: 'cheese' };
    service.setListListParams(listParams);

    const requestSearchWord = service.getRequestSearchTerm();

    expect(requestSearchWord).toBe('cheese');
  });
});
