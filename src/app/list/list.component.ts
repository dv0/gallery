import { Component } from '@angular/core';

import { ListService } from './service/list.service';

import { IGif } from '../core/model/data.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent {
  public listData$ = this.listService.listData$;

  constructor(private listService: ListService) {}

  public getGifUrl(gif: IGif): string {
    return gif.images.fixed_height.url;
  }
}
