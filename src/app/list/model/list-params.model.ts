export interface ListParams {
  page: number;
  searchTerm: string | null;
}
