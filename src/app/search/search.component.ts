import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ListService } from '../list/service/list.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent {
  public searchForm = new FormGroup({
    searchTerm: new FormControl('', Validators.required),
  });

  constructor(private listService: ListService) {}

  public search(): void {
    this.listService.setListListParams({
      page: 1,
      searchTerm: this.searchForm.value.searchTerm,
    });
  }

  public clear(): void {
    this.searchForm.reset();
    this.search();
  }
}
