import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListService } from '../list/service/list.service';

import { SearchComponent } from './search.component';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let listServiceSpy: jasmine.SpyObj<ListService>;

  beforeEach(async () => {
    listServiceSpy = jasmine.createSpyObj('ListService', ['setListListParams']);

    await TestBed.configureTestingModule({
      declarations: [SearchComponent],
      providers: [
        {
          provide: ListService,
          useValue: listServiceSpy,
        },
      ],
    }).compileComponents();

    listServiceSpy = TestBed.inject(ListService) as jasmine.SpyObj<ListService>;
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should emit search term with value from form', () => {
    const searchTerm = 'testing';

    component.searchForm.controls['searchTerm'].setValue(searchTerm);
    component.search();

    expect(listServiceSpy.setListListParams).toHaveBeenCalledTimes(1);
    expect(listServiceSpy.setListListParams).toHaveBeenCalledWith({
      page: 1,
      searchTerm: searchTerm,
    });
  });

  it('should reset form and search term', () => {
    const searchTerm = 'testing';

    component.searchForm.controls['searchTerm'].setValue(searchTerm);
    component.clear();

    expect(listServiceSpy.setListListParams).toHaveBeenCalledTimes(1);
    expect(listServiceSpy.setListListParams).toHaveBeenCalledWith({
      page: 1,
      searchTerm: null,
    });
  });
});
