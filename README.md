# Gallery

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Pre-commit task
- Added ESLint, Husky and Prettier to the project in order to execute a very complete pre-commit task, which include running tests.

## What is done?
- Using ng-bootstrap and scss to add styles.
- Simple image grid, populated by connecting to the Giphy API.
- Added search engine and pagination.
- 100% unit test coverage.

## Notes:
- When the search result returns a big amount of total results (e.g. on the initial response) and I tried to go to the last page, I noticed a weird behavior that seems to come from the API, recalculating the total amount of results to a much lower one and not returning any results for that specific last page. This is not happening on a smaller amount of results.
